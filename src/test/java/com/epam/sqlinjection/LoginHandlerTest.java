package com.epam.sqlinjection;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.OptionalLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LoginHandlerTest {

    @Test
    void testLoginHandlerSqlInjection() throws SQLException {

        ConnectionSource connectionSource = new ConnectionSource();
        Connection connection = connectionSource.getConnection();
        LoginHandler loginHandler = new LoginHandler();

        OptionalLong userId = loginHandler.login(connection, "Edgar'--", "unknown");

        //assertions fail - SQL Injection is prevented
        assertTrue(userId.isPresent());
        assertEquals(200L, userId.getAsLong());
    }

}